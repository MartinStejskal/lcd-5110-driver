/**
 * \file
 *
 * \brief Driver for LCD used in nokia 5110
 *
 * Used for basic operations (init, clear, write text, ...) with LCD 5510.
 * Emulate SPI bus.
 *
 * Created:  4.7.2012
 * Modified: 22.8.2013 (for 8 bit AVR)
 * \version 1.2
 * \author Martin Stejskal
 */


#ifndef _LCD_5110_H_
#define _LCD_5110_H_



/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
  // Control easily I/O
#include "ALPS_gpio_8bit.h"

#ifndef F_CPU
// F_CPU definition
#include "global_define.h"
#endif

// For delays
#include <util/delay.h>
// Data types
#include <inttypes.h>

// Because of strcpy
#include <string.h>

/*-----------------------------------------*
 |               Definitions               |
 *-----------------------------------------*/
/**
 * \name Library support configuration
 *
 * Allow configure additional pins. It is possible control additional pins by\n
 * AVR, or just set them by external components.
 *
 * @}
 *
 * \brief If true, CE will be controlled by AVR. Else pin is not used
 */
#define N5110_CE_support            false
//! \brief If true, RST will be controlled by AVR. Else pin is not used
#define N5110_RST_support           true
//! \brief If true, LIGHT will be controlled by AVR. Else pin is not used
#define N5110_LIGHT_support         false
/**
 * @}
 *
 * \name SW connections for Nokia LCD 5510
 *
 * @{
 */
//! \brief Serial Data IN
#define N5110_SDIN_PIN            gpio3_pin
#define N5110_SDIN_PORT           gpio3_port

//! \brief CLK
#define N5110_CLK_PIN             gpio4_pin
#define N5110_CLK_PORT            gpio4_port

//! \brief Data/!Command
#define N5110_DC_PIN              gpio2_pin
#define N5110_DC_PORT             gpio2_port
/**
 * @}
 *
 * \name Additional pin mapping (can be done by external HW)
 *
 * @}
 */
//! \brief Chip enable - active LOW
#define N5110_CE_PIN              gpio1_pin
#define N5110_CE_PORT             gpio1_port

//! \brief  Reset - active LOW
#define N5110_RST_PIN             gpio0_pin
#define N5110_RST_PORT            gpio0_port

//! \brief Background light - active low
#define N5110_BG_LIGHT_PIN        gpio5_pin
#define N5110_BG_LIGHT_PORT       gpio5_port

/**
 * @}
 *
 * \name Delays
 *
 * @{
 *
 * \brief Delay between two frames when communicate with LCD
 */
#define N5110_frame_delay         _delay_us(10)

//! \brief Delay after RESET goes to low level
#define N5110_reset_delay         _delay_ms(1)

//! \brief 1/2 of CLK period for LCD
#define N5110_clk_period          _delay_us(1)

//! @}

//! \brief Define maximum size of text buffer. Sometimes is
#define N5110_MAX_TEXT_BUFFER		84
/*-----------------------------------------*
 |           Global variables              |
 *-----------------------------------------*/
/**
 * \brief Counts "lines" on LCD
 *
 * Counts "lines" on LCD, so next word can be easy written to next line\n
 * (easy to calculate).
 */
volatile static uint8_t i_LCD_line_counter;

/**
 * \brief Determine X position on LCD
 */
volatile static uint8_t i_LCD_x_position;


/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/
//-----------------------------------------------------------------------------
// Basic functions
//-----------------------------------------------------------------------------
void LCD_init(void);
void LCD_clear(void);
#define LCD_write(p_txt_to_show)	LCD_write_string(p_txt_to_show)	// Compatibility
void LCD_write_string(char * p_txt_to_show);
void LCD_write_string_xy(char * p_txt_to_show,
		uint8_t x_position, uint8_t y_line);
void LCD_write_uint8_dec(uint8_t i_number);
void LCD_write_uint8_dec_xy(
		uint8_t i_number,
		uint8_t x_position,
		uint8_t y_line);

void LCD_write_uint8_hex(uint8_t i_number);
void LCD_write_uint8_hex_xy(
		uint8_t i_number,
		uint8_t x_position,
		uint8_t y_line);

void LCD_write_uint16_hex(uint16_t i_number);
void LCD_write_uint16_hex_xy(
		uint16_t i_number,
		uint8_t x_position,
		uint8_t y_line);



void LCD_write_string_and_uint8_dec(char * p_txt_to_show, uint8_t i_number);
void LCD_write_string_and_uint8_dec_xy(
		char * p_txt_to_show,
		uint8_t i_number,
		uint8_t x_position, uint8_t y_line);

//-----------------------------------------------------------------------------
// Advanced functions (when user do not want use sprintf and so on)
//-----------------------------------------------------------------------------
void LCD_add_uint8_dec_to_string(char * p_string, uint8_t i_number);
void LCD_uint8_dec_to_string(char * p_string, uint8_t i_number);

void LCD_add_uint8_hex_to_string(char * p_string, uint8_t i_number);
void LCD_uint8_hex_to_string(char * p_string, uint8_t i_number);
//-----------------------------------------------------------------------------
// Low level functions
//-----------------------------------------------------------------------------
void LCD_set_X(uint8_t X_address);
void LCD_set_line(uint8_t y_line);

void LCD_send_byte(uint8_t data);

void LCD_add_digit_dec_to_str(		uint8_t i_digit,
									uint8_t i_index,
									char * p_string);

void LCD_add_digit_hex_to_str(		uint8_t i_digit,
									uint8_t i_index,
									char * p_string);



// Just for case....
#ifndef false
#define false   0
#endif
#ifndef true
#define true    1
#endif


///\todo Doxygen doc for functions
#endif
