/**
 * \file
 *
 * \brief Driver for LCD used in nokia 5110
 *
 * Used for basic operations (init, clear, write text, ...) with LCD 5110.
 * Emulate SPI bus. Resolution: 84x48px -> 14x6 symbols
 *
 * Created:  4.7.2012
 * Modified: 22.8.2013 (for 8 bit AVR)
 * \version 1.2
 * \author Martin Stejskal
 */

// Library, created for writing infos to LCD
#include "LCD_5110_SPI_SW_emulated_8bit.h"


// Font for LCD nokia 5110
#ifndef _FONT_NOKIA_5110_
#define _FONT_NOKIA_5110_
//! This is binary appearance all symbols in array
const unsigned char n5110_font6x8[][6] =
{
{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },   // sp	   32
{ 0x00, 0x00, 0x00, 0x2f, 0x00, 0x00 },   // !	   33
{ 0x00, 0x00, 0x07, 0x00, 0x07, 0x00 },   // "	   34
{ 0x00, 0x14, 0x7f, 0x14, 0x7f, 0x14 },   // #	   35
{ 0x00, 0x24, 0x2a, 0x7f, 0x2a, 0x12 },   // $	   36
{ 0x00, 0x62, 0x64, 0x08, 0x13, 0x23 },   // %	   37
{ 0x00, 0x36, 0x49, 0x55, 0x22, 0x50 },   // &	   38
{ 0x00, 0x00, 0x05, 0x03, 0x00, 0x00 },   // '	   39
{ 0x00, 0x00, 0x1c, 0x22, 0x41, 0x00 },   // (	   40
{ 0x00, 0x00, 0x41, 0x22, 0x1c, 0x00 },   // )	   41
{ 0x00, 0x14, 0x08, 0x3E, 0x08, 0x14 },   // *	   42
{ 0x00, 0x08, 0x08, 0x3E, 0x08, 0x08 },   // +	   43
{ 0x00, 0x00, 0x00, 0xA0, 0x60, 0x00 },   // ,	   44
{ 0x00, 0x08, 0x08, 0x08, 0x08, 0x08 },   // -	   45
{ 0x00, 0x00, 0x60, 0x60, 0x00, 0x00 },   // .	   46
{ 0x00, 0x20, 0x10, 0x08, 0x04, 0x02 },   // /	   47
{ 0x00, 0x3E, 0x51, 0x49, 0x45, 0x3E },   // 0	   48
{ 0x00, 0x00, 0x42, 0x7F, 0x40, 0x00 },   // 1	   49
{ 0x00, 0x42, 0x61, 0x51, 0x49, 0x46 },   // 2	   50
{ 0x00, 0x21, 0x41, 0x45, 0x4B, 0x31 },   // 3	   51
{ 0x00, 0x18, 0x14, 0x12, 0x7F, 0x10 },   // 4	   52
{ 0x00, 0x27, 0x45, 0x45, 0x45, 0x39 },   // 5	   53
{ 0x00, 0x3C, 0x4A, 0x49, 0x49, 0x30 },   // 6	   54
{ 0x00, 0x01, 0x71, 0x09, 0x05, 0x03 },   // 7	   55
{ 0x00, 0x36, 0x49, 0x49, 0x49, 0x36 },   // 8	   56
{ 0x00, 0x06, 0x49, 0x49, 0x29, 0x1E },   // 9	   57
{ 0x00, 0x00, 0x36, 0x36, 0x00, 0x00 },   // :	   58
{ 0x00, 0x00, 0x56, 0x36, 0x00, 0x00 },   // ;	   59
{ 0x00, 0x08, 0x14, 0x22, 0x41, 0x00 },   // <	   60
{ 0x00, 0x14, 0x14, 0x14, 0x14, 0x14 },   // =	   61
{ 0x00, 0x00, 0x41, 0x22, 0x14, 0x08 },   // >	   62
{ 0x00, 0x02, 0x01, 0x51, 0x09, 0x06 },   // ?	   63
{ 0x00, 0x32, 0x49, 0x59, 0x51, 0x3E },   // @	   64
{ 0x00, 0x7C, 0x12, 0x11, 0x12, 0x7C },   // A	   65
{ 0x00, 0x7F, 0x49, 0x49, 0x49, 0x36 },   // B	   66
{ 0x00, 0x3E, 0x41, 0x41, 0x41, 0x22 },   // C	   67
{ 0x00, 0x7F, 0x41, 0x41, 0x22, 0x1C },   // D	   68
{ 0x00, 0x7F, 0x49, 0x49, 0x49, 0x41 },   // E	   69
{ 0x00, 0x7F, 0x09, 0x09, 0x09, 0x01 },   // F	   70
{ 0x00, 0x3E, 0x41, 0x49, 0x49, 0x7A },   // G	   71
{ 0x00, 0x7F, 0x08, 0x08, 0x08, 0x7F },   // H	   72
{ 0x00, 0x00, 0x41, 0x7F, 0x41, 0x00 },   // I	   73
{ 0x00, 0x20, 0x40, 0x41, 0x3F, 0x01 },   // J	   74
{ 0x00, 0x7F, 0x08, 0x14, 0x22, 0x41 },   // K	   75
{ 0x00, 0x7F, 0x40, 0x40, 0x40, 0x40 },   // L	   76
{ 0x00, 0x7F, 0x02, 0x0C, 0x02, 0x7F },   // M	   77
{ 0x00, 0x7F, 0x04, 0x08, 0x10, 0x7F },   // N	   78
{ 0x00, 0x3E, 0x41, 0x41, 0x41, 0x3E },   // O	   79
{ 0x00, 0x7F, 0x09, 0x09, 0x09, 0x06 },   // P	   80
{ 0x00, 0x3E, 0x41, 0x51, 0x21, 0x5E },   // Q	   81
{ 0x00, 0x7F, 0x09, 0x19, 0x29, 0x46 },   // R	   82
{ 0x00, 0x46, 0x49, 0x49, 0x49, 0x31 },   // S	   83
{ 0x00, 0x01, 0x01, 0x7F, 0x01, 0x01 },   // T	   84
{ 0x00, 0x3F, 0x40, 0x40, 0x40, 0x3F },   // U	   85
{ 0x00, 0x1F, 0x20, 0x40, 0x20, 0x1F },   // V	   86
{ 0x00, 0x3F, 0x40, 0x38, 0x40, 0x3F },   // W	   87
{ 0x00, 0x63, 0x14, 0x08, 0x14, 0x63 },   // X	   88
{ 0x00, 0x07, 0x08, 0x70, 0x08, 0x07 },   // Y	   89
{ 0x00, 0x61, 0x51, 0x49, 0x45, 0x43 },   // Z	   90
{ 0x00, 0x00, 0x7F, 0x41, 0x41, 0x00 },   // [	   91
{ 0x00, 0x55, 0x2A, 0x55, 0x2A, 0x55 },   // 55	   92
{ 0x00, 0x00, 0x41, 0x41, 0x7F, 0x00 },   // ]	   93
{ 0x00, 0x04, 0x02, 0x01, 0x02, 0x04 },   // ^	   94
{ 0x00, 0x40, 0x40, 0x40, 0x40, 0x40 },   // _	   95
{ 0x00, 0x00, 0x01, 0x02, 0x04, 0x00 },   // '	   96
{ 0x00, 0x20, 0x54, 0x54, 0x54, 0x78 },   // a	   97
{ 0x00, 0x7F, 0x48, 0x44, 0x44, 0x38 },   // b	   98
{ 0x00, 0x38, 0x44, 0x44, 0x44, 0x20 },   // c	   99
{ 0x00, 0x38, 0x44, 0x44, 0x48, 0x7F },   // d	   100
{ 0x00, 0x38, 0x54, 0x54, 0x54, 0x18 },   // e	   101
{ 0x00, 0x08, 0x7E, 0x09, 0x01, 0x02 },   // f	   102
{ 0x00, 0x18, 0xA4, 0xA4, 0xA4, 0x7C },   // g	   103
{ 0x00, 0x7F, 0x08, 0x04, 0x04, 0x78 },   // h	   104
{ 0x00, 0x00, 0x44, 0x7D, 0x40, 0x00 },   // i	   105
{ 0x00, 0x40, 0x80, 0x84, 0x7D, 0x00 },   // j	   106
{ 0x00, 0x7F, 0x10, 0x28, 0x44, 0x00 },   // k	   107
{ 0x00, 0x00, 0x41, 0x7F, 0x40, 0x00 },   // l	   108
{ 0x00, 0x7C, 0x04, 0x18, 0x04, 0x78 },   // m	   109
{ 0x00, 0x7C, 0x08, 0x04, 0x04, 0x78 },   // n	   110
{ 0x00, 0x38, 0x44, 0x44, 0x44, 0x38 },   // o	   111
{ 0x00, 0xFC, 0x24, 0x24, 0x24, 0x18 },   // p	   112
{ 0x00, 0x18, 0x24, 0x24, 0x18, 0xFC },   // q	   113
{ 0x00, 0x7C, 0x08, 0x04, 0x04, 0x08 },   // r	   114
{ 0x00, 0x48, 0x54, 0x54, 0x54, 0x20 },   // s	   115
{ 0x00, 0x04, 0x3F, 0x44, 0x40, 0x20 },   // t	   116
{ 0x00, 0x3C, 0x40, 0x40, 0x20, 0x7C },   // u	   117
{ 0x00, 0x1C, 0x20, 0x40, 0x20, 0x1C },   // v	   118
{ 0x00, 0x3C, 0x40, 0x30, 0x40, 0x3C },   // w	   119
{ 0x00, 0x44, 0x28, 0x10, 0x28, 0x44 },   // x	   120
{ 0x00, 0x1C, 0xA0, 0xA0, 0xA0, 0x7C },   // y	   121
{ 0x00, 0x44, 0x64, 0x54, 0x4C, 0x44 },   // z	   122
{ 0x14, 0x14, 0x14, 0x14, 0x14, 0x14 }    // horiz lines	123
};


#endif


//-----------------------------------------------------------------------------
// Basic functions
//-----------------------------------------------------------------------------
/**
 * Initialize LCD and call clear screen function
 */
void LCD_init(void){
	// Set I/O

	// SDIN - out, low level
	io_set_dir_out(N5110_SDIN_PORT, N5110_SDIN_PIN);
	io_set_L(      N5110_SDIN_PORT, N5110_SDIN_PIN);
	// SCLK - out, low level
	io_set_dir_out(N5110_CLK_PORT, N5110_CLK_PIN);
	io_set_L(      N5110_CLK_PORT, N5110_CLK_PIN);
	// DC - out, low level
	io_set_dir_out(N5110_DC_PORT, N5110_DC_PIN);
	io_set_L(      N5110_DC_PORT, N5110_DC_PIN);

	// Other pins will be used only if support is enabled (true)

#if N5110_CE_support == true
	// CE - output, low level
	io_set_dir_out(N5110_CE_PORT, N5110_CE_PIN);
	io_set_L(      N5110_CE_PORT, N5110_CE_PIN);
#endif

#if N5110_RST_support == true
	// RST- output
	io_set_dir_out(N5110_RST_PORT, N5110_RST_PIN);
	// Set low level -> restart LCD

	io_set_L(      N5110_RST_PORT, N5110_RST_PIN);
	// Short delay for restart LCD

	N5110_reset_delay;

	// Set High level -> LCD will be ready to communicate
	io_set_H(      N5110_RST_PORT, N5110_RST_PIN);
#else // Reset support off -> it is up to hardware -> do not know -> long delay
	N5110_reset_delay;
	N5110_reset_delay;
	N5110_reset_delay;
	N5110_reset_delay;
	N5110_reset_delay;
	N5110_reset_delay;
	N5110_reset_delay;
	N5110_reset_delay;
	N5110_reset_delay;
	N5110_reset_delay;
#endif




    // Init sequence - DATA/!CMD is already set to 0

	// last: 0x0C - normal mode LCD, 0x08 - blank LCD
	uint8_t data[]={0x21, 0xC0, 0x06, 0x13, 0x20, 0x0C};
	for (uint8_t i=0 ; i<6 ; i++)
	{
		LCD_send_byte(data[i]);	// Write data[]
		N5110_frame_delay;			// Delay
	}


	// DATA

	// DC high -> now  can be written just data
	io_set_H(N5110_DC_PORT, N5110_DC_PIN);

	/* Now it is LCD ready to Rx data. When D/!C value is changed, then there
	 * must be time space because of LCD read D/!C value after last bit is
	 * transmitted
	 */
	N5110_frame_delay;


	// Clear nokia 5110 display
	LCD_clear();

#if N5110_LIGHT_support == true
	// LIGHT - output
	io_set_dir_out(N5110_BG_LIGHT_PORT, N5110_BG_LIGHT_PIN);
	// And on background light
	io_set_L(      N5110_BG_LIGHT_PORT, N5110_BG_LIGHT_PIN);
#endif


	// And disable chip select on nokia 5110 LCD - already done by LCD_clear
};


/**
 * Clear screen and set cursor to 0,0. This function is called by "LCD_init"
 */
void LCD_clear(void)
{
#if N5110_CE_support == true
	// CE - output, low level
	io_set_L(N5110_CE_PORT, N5110_CE_PIN);
#endif

	// And clear N5110 LCD
	for (int i=0 ; i<504 ; i++)	// (48x84)/8=504
	{
		// Write 0x00, witch means "all showed bits to zero"
		LCD_send_byte(0x00);

		// There must be delay between "two frames"
		N5110_frame_delay;
	}
	
	// And set position coordinates on LCD to 0,0
	LCD_set_X(0);
	LCD_set_line(0);

#if N5110_CE_support == true
	// CE - output, high level
	io_set_H(N5110_CE_PORT, N5110_CE_PIN);
#endif

	// And set inform coordinates (use in future)
	i_LCD_line_counter = 0;	// Line is zero ("first" line on real display)
	i_LCD_x_position = 0;	// X coordinate is set to begin -> zero
};

/**
 * Write text to LCD. Text is given as pointer to char. Write characters until
 * is found NULL. If message is not fit on one line, then message continues on
 * the next line
 * \param p_txt_to_show		Pointer to string that ends NULL character
 *
 * Example of using "LCD_write":\n
 * \n
 *  i_status = some_function();\n
 *  // Needed include lib. "stdio.h" for sprintf\n
 *  sprintf(text, "Codec set status: %d", i_status);\n
 *  LCD_write_string( &text );\n
 * \n
 * Or:\n
 * \n
 *  LCD_write_string("Text to show");
 */
void LCD_write_string(char * p_txt_to_show)
{
#if N5110_CE_support == true
	// CE - output, low level
	io_set_L(N5110_CE_PORT, N5110_CE_PIN);
#endif

	// for counting pixels, that are already written to LCD
	uint8_t i;
	// Symbol ASCII number is recalculated
	uint8_t symbol_ascii_number;

	// Test line number
	if (i_LCD_line_counter > 5)	// Should not be bigger than 5 -> fail
	{
		i_LCD_line_counter = 0;
		LCD_set_line( i_LCD_line_counter );
		LCD_clear();
	}

	// Print symbols, until is found NULL character -> end of string
	while ( *p_txt_to_show != '\0' )
	{
		/* Test if actual symbol is '\n' -> this means "jump to next line" ->
		 * -> must change line number
		 */
		if ( (*(p_txt_to_show) == '\n' ))
		{
			i_LCD_x_position = 0;	// X position on zero

			// And change number of actual line (plus one -> move to next)
			i_LCD_line_counter++;

			LCD_set_X(0);				// Set X address in LCD to zero

#if N5110_CE_support == true
			/* LCD_debug_5110_set_X disable "chip enable", so must be enabled
			 * again
			 */
			// CE - output, low level
			io_set_L(N5110_CE_PORT, N5110_CE_PIN);
#endif

			LCD_set_line(i_LCD_line_counter);	// And move to next line

#if N5110_CE_support == true
			// Same, chip enable must be enabled again
			// CE - output, low level
			io_set_L(N5110_CE_PORT, N5110_CE_PIN);
#endif

			/* and move pointer forward by one (to next character, we won't
			 * write line feed on LCD)
			 */
			p_txt_to_show++;
		}


		/* Test if next symbol fit on actual line or not. If not, then jump to
		 * next line and set X coordinates to 0
		 */
		if (i_LCD_x_position > (83-6+1) )
		/* 83 is maximum address, 6 is symbol length however, when to 0 is
		 * added 6 -> new X position is 6, but it should be 5 (cause we started
		 * from zero) -> so that is reason +1 offset
		 */
		{
			i_LCD_x_position = 0;			// Set X address to zero
			i_LCD_line_counter++;			// Move to next line

			LCD_set_X(0);						// Set X to zero
#if N5110_CE_support == true
			/* LCD_debug_5110_set_X disabled "chip enable", so must be enabled
			 * again
			 */
			// CE - output, low level
			io_set_L(N5110_CE_PORT, N5110_CE_PIN);
#endif
			LCD_set_line(i_LCD_line_counter);	// And move to next line
#if N5110_CE_support == true
			// Same, chip enable must be enabled again
			// CE - output, low level
			io_set_L(N5110_CE_PORT, N5110_CE_PIN);
#endif
		}


		/* Calculate from ASCII code number corresponding with "n5110_font6x8"
		 * database
		 */
		// Get ASCII value from actual symbol (font begins at 32 ASCII character)
		symbol_ascii_number = *(p_txt_to_show++) -32;
		for (i=0 ; i<6 ; i++)	// every symbol is 6 points wide
		{
			LCD_send_byte(n5110_font6x8[symbol_ascii_number][i]);
			N5110_frame_delay;	// And put some delay between next bits
		}

		/* After write symbol -> i_LCD_x_position should be increased by 6
		 * (symbol length)
		 */
		i_LCD_x_position = i_LCD_x_position + 6;
	}

#if N5110_CE_support == true
	// And disable chip select on nokia 5110 LCD
	// CE - output, high level
	io_set_H(N5110_CE_PORT, N5110_CE_PIN);
#endif

	// And move to next line -> for next call of this function
	i_LCD_line_counter++;

	// When we came to the last line
	if(i_LCD_line_counter == 6)
	{/* Do nothing...
	  * If this function will be called again, in the begin find out, that
	  * i_LCD_line_counter is greater than 5 -> clear screen and set right
	  * line
	  */
	}
	else
	{// Else i_LCD_line_counter is not 6 -> standard procedure
		LCD_set_line(i_LCD_line_counter);
	}
	i_LCD_x_position = 0;
	LCD_set_X(0);
}

/**
 * Write text to LCD at specified coordinates. Text must end with NULL
 * character. If message is not fit on one line, then message continues on the
 * next line.
 * \param p_txt_to_show		Pointer to string that ends NULL character
 * \param x_position		X coordinate. Accepted value 0~83 else is set to 0
 * \param y_line			Line index on LCD. Accepted value 0~5 else is set
 * to 0
 *
 * Example of using "LCD_write_string_xy":\n
 * \n
 *  i_status = some_function();\n
 *  // Needed include lib. "stdio.h" for sprintf\n
 *  sprintf(text, "Codec set status: %d", i_status);\n
 *  LCD_write_string_xy( &text, 3, 1 );\n
 *
 * Or:
 *
 *  LCD_write_string_xy("Text to show", 10, 0);
 */
void LCD_write_string_xy(char * p_txt_to_show,
		uint8_t x_position, uint8_t y_line)
{
	/* If You like counting started 1, instead of 0 -> uncomment following two
	 * lines
	 */
	//y_line--;		// LCDs calculate lines by 0 -> minus one
	//x_position--;	// again, for LCD is begin 0

	// Coordinates set, so set them on N5110 LCD
    LCD_set_X(x_position);
	LCD_set_line(y_line);

	// And finally write data on LCD(s)
	LCD_write_string( p_txt_to_show);
}

/**
 * Write number to LCD. Internally convert number to string and then use\n
 * function LCD_write_string. Faster, request lower memory  than \b sprintf \n
 * function.
 * @param i_number Unsigned 8 bit integer
 *
 * Example of using "LCD_write_uint8":\n
 * \n
 * i_status = some_function();\n
 * LCD_write_uint8(i_status);
 */
void LCD_write_uint8_dec(uint8_t i_number)
{
	// Temporary variable - save digits
	char c_text[4];

	// uint8 to string
	LCD_uint8_dec_to_string(c_text, i_number);

	// Write data to LCD
	LCD_write_string(c_text);
}


/**
 * Write number to LCD to specific coordinates. Internally convert number to\n
 * string and then use function LCD_write_string_xy. Faster, request lower\n
 * memory  than \b sprintf function.
 * @param i_number Unsigned 8 bit integer
 * \param x_position		X coordinate. Accepted value 0~83 else is set to 0
 * \param y_line			Line index on LCD. Accepted value 0~5 else is set
 * to 0
 *
 * Example of using "LCD_write_uint8_xy":\n
 *\n
 * i_status = some_function();\n
 * // Write number to line 2 and 35th point\n
 * LCD_write_uint8_xy(i_status, 35, 2);
 */
void LCD_write_uint8_dec_xy(uint8_t i_number, uint8_t x_position, uint8_t y_line)
{
	// Temporary variable - save digits
	char c_text[4];

	// uint8 to string
	LCD_uint8_dec_to_string(c_text, i_number);

	// Write data to LCD
	LCD_write_string_xy(c_text, x_position, y_line);
}


void LCD_write_uint8_hex(uint8_t i_number)
{
	// Temporary variable - save digits
	char c_text[5] = {"0x"};

	// uint8 to string
	LCD_uint8_hex_to_string(&(c_text[2]), i_number);

	// Write data to LCD
	LCD_write_string(c_text);
}

void LCD_write_uint8_hex_xy(
		uint8_t i_number,
		uint8_t x_position,
		uint8_t y_line)
{
	// Temporary variable - save digits
	char c_text[5] = {"0x"};

	// uint8 to string
	LCD_uint8_hex_to_string(&(c_text[2]), i_number);

	// Write data to LCD
	LCD_write_string_xy(c_text, x_position, y_line);
}

void LCD_write_uint16_hex(uint16_t i_number)
{
	// Temporary variable - save digits
	char c_text[7] = {"0x"};

	// Add high 8bits to string
	LCD_uint8_hex_to_string(&(c_text[2]), (i_number>>8));

	// Add low 8bits
	LCD_uint8_hex_to_string(&(c_text[4]), (i_number & 0xFF));

	// Write data to LCD
	LCD_write_string(c_text);
}

void LCD_write_uint16_hex_xy(
		uint16_t i_number,
		uint8_t x_position,
		uint8_t y_line)
{
	// Temporary variable - save digits
	char c_text[7] = {"0x"};

	// Add high 8bits to string
	LCD_uint8_hex_to_string(&(c_text[2]), (i_number>>8));

	// Add low 8bits
	LCD_uint8_hex_to_string(&(c_text[4]), (i_number & 0xFF));

	// Write data to LCD
	LCD_write_string_xy(c_text, x_position, y_line);
}


/**
 * \brief Find end of string and add 8 bit value. Whole string is printed
 *
 * Algorithm find NULL character in string and add number and new NULL\n
 * character. Then whole string write to LCD
 * @param p_txt_to_show Pointer to string array
 * @param i_number 8 bit unsigned number
 *
 * Example of using "LCD_write_string_and_uint8":\n
 * \n
 * // Minimal size: string (4), number (3) and NULL (1)\n
 * char c_text[8] = "ABCD";\n
 * LCD_write_string_and_uint8(c_text, 45);\n
 * // On LCD appears: "ABCD45"
 */
void LCD_write_string_and_uint8_dec(char * p_txt_to_show, uint8_t i_number)
{
	/* Temporary string buffer. It can happen, that user call function like
	 * this:
	 * LCD_write_string_and_uint8("Hello ", 5);
	 * But buffer would overflows -> copy to buffer big enough
	 */
	char c_temp_text[N5110_MAX_TEXT_BUFFER];
	strcpy(c_temp_text, p_txt_to_show);

	LCD_add_uint8_dec_to_string(c_temp_text, i_number);
	LCD_write_string(c_temp_text);
}


/**
 * \brief Find end of string and add 8 bit value. Whole string is printed.\n
 * Also is possible set coordinates
 *
 * Algorithm find NULL character in string and add number and new NULL\n
 * character. Then whole string write to LCD on specific coordinates
 * @param p_txt_to_show Pointer to string array
 * @param i_number 8 bit unsigned number
 * \param x_position		X coordinate. Accepted value 0~83 else is set to 0
 * \param y_line			Line index on LCD. Accepted value 0~5 else is set
 * to 0
 *
 * Example of using "LCD_write_string_and_uint8":\n
 * \n
 * // Minimal size: string (4), number (3) and NULL (1)\n
 * char c_text[8] = "ABCD";\n
 * LCD_write_string_and_uint8(c_text, 45);\n
 * // On LCD appears: "ABCD45"
 */
inline void LCD_write_string_and_uint8_dec_xy(char * p_txt_to_show, uint8_t i_number,
		uint8_t x_position, uint8_t y_line)
{
	/* Temporary string buffer. It can happen, that user call function like
	 * this:
	 * LCD_write_string_and_uint8("Hello ", 5);
	 * But buffer would overflows -> copy to buffer big enough
	 */
	char c_temp_text[N5110_MAX_TEXT_BUFFER];
	strcpy(c_temp_text, p_txt_to_show);

	LCD_add_uint8_dec_to_string(c_temp_text, i_number);
	LCD_write_string_xy(c_temp_text, x_position, y_line);
}
//-----------------------------------------------------------------------------
// Advanced functions (when user do not want use sprintf and so on)
//-----------------------------------------------------------------------------
/**
 * \brief Find end of string and add 8 bit value as decadic number
 *
 * Algorithm find NULL character in string and add number and new NULL\n
 * character
 * @param p_string Pointer to string array
 * @param i_number 8 bit unsigned number
 *
 * Example of using "LCD_add_uint8_to_string":\n
 * \n
 * // Minimal size: string (4), number (3) and NULL (1)\n
 * char c_text[8] = "ABCD";\n
 * LCD_add_uint8_dec_to_string(c_text, 13);\n
 * // Now in array c_text we have "ADCB13"
 */
inline void LCD_add_uint8_dec_to_string(char * p_string, uint8_t i_number)
{
	// Find NULL character
	while( *p_string != 0x00 )
	{
		p_string++;
	}

	// When found, then calculate number
	LCD_uint8_dec_to_string(p_string, i_number);
}

/**
 * \brief Change uint8 to string and write it to p_string (add NULL\n
 * character). Output number is decadic
 * @param p_string Pointer to string array
 * @param i_number 8 bit unsigned number
 *
 * Example of using "LCD_uint8_dec_to_string":\n
 * \n
 * // Minimal size: value (3) and NULL (1) character\n
 * char c_text[4];\n
 * LCD_uint8_dec_to_string(c_text, 35);\n
 * // Now in array c_text we have "35"
 */
void LCD_uint8_dec_to_string(char * p_string, uint8_t i_number)
{
	// Test if number is higher or equal 100
	if(i_number >= 100)
	{
		// Count decades
		uint8_t i_decades = 0;
		// Temporary variable for iterations
		uint8_t i_tmp;

		// Calculate number of hundreds - there are only 2 options: 100, 200
		if(i_number >= 200)
		{
			// Write "2"
			LCD_add_digit_dec_to_str(2, 0, p_string);
			// Get only decades
			i_tmp = i_number -200;
		}
		else
		{
			// Write "1"
			LCD_add_digit_dec_to_str(1, 0, p_string);
			// Get only decades
			i_tmp = i_number -100;
		}

		// Calculate number of decades
		while(i_tmp >= 10)
		{
			i_tmp = i_tmp - 10;
			i_decades++;
		}

		// So now in i_decades we have number of decades from input number
		LCD_add_digit_dec_to_str(i_decades, 1, p_string);
		// Rest in i_tmp are units
		LCD_add_digit_dec_to_str(i_tmp,     2, p_string);

		return;
	}

	if(i_number >= 10)
	{
		// Count decades
		uint8_t i_decades = 0;
		// Temporary variable for iterations
		uint8_t i_tmp= i_number;

		// Calculate number of decades
		while(i_tmp >= 10)
		{
			i_tmp = i_tmp - 10;
			i_decades++;
		}

		// So now in i_decades we have number of decades from input number
		LCD_add_digit_dec_to_str(i_decades, 0, p_string);
		// Rest in i_tmp are units
		LCD_add_digit_dec_to_str(i_tmp,     1, p_string);
	}
	else
	{
		LCD_add_digit_dec_to_str(i_number, 0, p_string);
	}

}


inline void LCD_add_uint8_hex_to_string(char * p_string, uint8_t i_number)
{
	// Find NULL character
	while( *p_string != 0x00 )
	{
		p_string++;
	}

	// When found, then calculate number
	LCD_uint8_hex_to_string(p_string, i_number);
}


inline void LCD_uint8_hex_to_string(char * p_string, uint8_t i_number)
{
	// MSB nibble first
	LCD_add_digit_hex_to_str( (i_number>>4), 0, p_string);

	// LSB nibble next
	LCD_add_digit_hex_to_str( (i_number & 0x0F), 1, p_string);
}

//-----------------------------------------------------------------------------
// Low level functions
//-----------------------------------------------------------------------------
/**
 * Only set X coordinate on LCD nokia 5110
 * \param X_address			 X coordinate. Accepted value 0~83 else is set to 0
 */
inline void LCD_set_X(uint8_t X_address)
{
	// Test for input variable
	if (X_address > 83)	// Should not be bigger than 83 -> fail
	{
		X_address = 0;
	}
#if N5110_CE_support == true
	// CE - output, low level
	io_set_L(N5110_CE_PORT, N5110_CE_PIN);
#endif

	// DC low -> now will be written command
	io_set_L(N5110_DC_PORT, N5110_DC_PIN);


	// Set X-address, command is 0x80 -> result command is (0x80 | X_address)
	LCD_send_byte( X_address | 0x80 );

	N5110_frame_delay;

	// DC high -> now can be written just data
	io_set_H(N5110_DC_PORT, N5110_DC_PIN);


	// Save information about X position
	i_LCD_x_position = X_address;

#if N5110_CE_support == true
	// And disable chip select on nokia 5110 LCD
	// CE - output, high level
	io_set_H(N5110_CE_PORT, N5110_CE_PIN);
#endif
}


/**
 * Only set active line on LCD nokia 5110
 * \param y_line			Line index on LCD. Accepted value 0~5 else is set
 */
inline void LCD_set_line(uint8_t y_line)
{
	/* Save information about Y position. Then program will work just with
	 * "i_LCD_line_counter"
	 */
	i_LCD_line_counter = y_line;

	// Test line number
	if (i_LCD_line_counter > 5)	// Should not be bigger than 5 -> fail
	{
		i_LCD_line_counter = 0;
		LCD_set_line( i_LCD_line_counter );
	}


#if N5110_CE_support == true
	// CE - output, low level
	io_set_L(N5110_CE_PORT, N5110_CE_PIN);
#endif
	// DC low -> now will be written command
	io_set_L(N5110_DC_PORT, N5110_DC_PIN);

	// Set Y-address, command is 0x40 -> result command is (0x40 | X_address)
	LCD_send_byte(i_LCD_line_counter | 0x40);
	N5110_frame_delay;

	// DC high -> now can be written just data
	io_set_H(N5110_DC_PORT, N5110_DC_PIN);


#if N5110_CE_support == true
	// And disable chip select on nokia 5110 LCD
	// CE - output, high level
	io_set_H(N5110_CE_PORT, N5110_CE_PIN);
#endif
}

/**
 *\brief Send 8 bits to LCD nokia 5110
 *
 * Low level function. Send raw data
 *
 * \param data Byte witch will be sent
 */
inline void LCD_send_byte(uint8_t data)
{
	for(uint8_t i=7 ; i<8 ; i--)
	{
		// Clock to LOW
		io_set_L(N5110_CLK_PORT, N5110_SDIN_PIN);

		// load "i" bit and check if is zero or not. Use masking and AND func.
		if( ( data & (1<<i) ) == 0)
		{// If zero
			io_set_0(N5110_SDIN_PORT, N5110_SDIN_PIN);
		}
		else
		{// Else one
			io_set_1(N5110_SDIN_PORT, N5110_SDIN_PIN);
		}

		// Time delay
		N5110_clk_period;

		io_set_H(N5110_CLK_PORT, N5110_CLK_PIN);
		// Time delay
		N5110_clk_period;
	}


	// Clock to LOW
	io_set_L(N5110_CLK_PORT, N5110_SDIN_PIN);
}

/**
 * \brief Add input digit to string on defined index and add NULL character
 * @param i_digit Digit. Number from 0 to 10. Else there will be character '?'
 * @param i_index Define where will be digit written.
 * @param p_string String array
 */
inline void LCD_add_digit_dec_to_str(	uint8_t i_digit,
										uint8_t i_index,
										char * p_string)
{
	if(i_digit>9)
	{
		p_string[i_index]   = '?';
		p_string[i_index+1] = 0x00;
	}
	else
	{
		// This done correct ASCII value
		p_string[i_index]  = i_digit + 48;
		p_string[i_index+1] = 0x00;
	}

}

/**
 * \brief Add input digit to string on defined index and add NULL character
 * @param i_digit Digit. Number from 0 to 0xF. Else there will be character '?'
 * @param i_index Define where will be digit written.
 * @param p_string String array
 */
inline void LCD_add_digit_hex_to_str(	uint8_t i_digit,
										uint8_t i_index,
										char* p_string)
{
	if(i_digit>9)
	{
		// Test for A,B,C,D,E and F (in hex are valid numbers)
		if(i_digit>0x0F)
		{
			// If higher than 0x0F -> FAIL
			p_string[i_index]   = '?';
			p_string[i_index+1] = 0x00;	// NULL
		}
		else
		{
			// Else it is hex number - get ASCII number
			p_string[i_index]   = i_digit + 55;
			p_string[i_index+1] = 0x00;	// NULL
		}
	}
	else
	{
		// This done correct ASCII value
		p_string[i_index]  = i_digit + 48;
		p_string[i_index+1] = 0x00;		// NULL
	}
}
